package me.dkzwm.widget.srl.sample.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import me.dkzwm.widget.srl.RefreshingListenerAdapter;
import me.dkzwm.widget.srl.WaveSmoothRefreshLayout;
import me.dkzwm.widget.srl.extra.IRefreshView;
import me.dkzwm.widget.srl.sample.R;

public class MainActivity extends AppCompatActivity {
    private Handler mHandler = new Handler();
    private WaveSmoothRefreshLayout mRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRefreshLayout = (WaveSmoothRefreshLayout) findViewById(R.id.smoothRefreshLayout_main);



        findViewById(R.id.start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRefreshLayout.autoRefresh(true, false);
                Log.d("MainActivity"," run start........");
                mRefreshLayout.setDisableLoadMore(true);
                mRefreshLayout.setEnableHideFooterView(true);
                mRefreshLayout.getDefaultHeader().setWaveColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                mRefreshLayout.getDefaultHeader().setStyle(IRefreshView.STYLE_DEFAULT);
            }
        });




        findViewById(R.id.stop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRefreshLayout.refreshComplete();
                Log.d("MainActivity"," run stop........");
            }
        });
//        mRefreshLayout.setOnRefreshListener(new RefreshingListenerAdapter() {
//            @Override
//            public void onRefreshBegin(boolean isRefresh) {
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                    }
//                }, 10000);
//            }
//        });

        mRefreshLayout.setOnRefreshListener(new RefreshingListenerAdapter() {
            @Override
            public void onRefreshBegin(boolean isRefresh) {
                mRefreshLayout.autoRefresh(true, false);
                Log.d("MainActivity"," run setOnRefreshListener........");
                mRefreshLayout.setDisableLoadMore(true);
                mRefreshLayout.setEnableHideFooterView(true);
                mRefreshLayout.getDefaultHeader().setWaveColor(ContextCompat.getColor(MainActivity.this, R.color.colorAccent));
                mRefreshLayout.getDefaultHeader().setStyle(IRefreshView.STYLE_DEFAULT);
            }
        });
//        mRefreshLayout.setDisableLoadMore(false);
//        mRefreshLayout.setDisablePerformLoadMore(true);
//        mRefreshLayout.setEnableHideFooterView(true);
////        mRefreshLayout.getDefaultHeader().setWaveColor(ContextCompat.getColor(this, R.color.colorPrimary));
////        mRefreshLayout.getDefaultHeader().setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
//        mRefreshLayout.getDefaultHeader().setStyle(IRefreshView.STYLE_DEFAULT);

    }



//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        mHandler.removeCallbacksAndMessages(null);
//    }

}
